/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

// Adicionando a funcao void run(Simulation & s, double dt)
void run(Simulation & s, double dt) {
  for (int i = 0; i < 100; ++i) { s.step(dt); s.display(); }
}

int main(int argc, char** argv)
{
  Ball ball;
  const double dt = 1.0/30;
  // Adicionando chamada para run
  run(ball, dt);
  // for (int i = 0; i < 100; ++i) {
  //   ball.step(dt);
  //   ball.display();
  // }
  return 0;
}
